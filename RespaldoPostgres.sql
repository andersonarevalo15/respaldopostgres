--
-- PostgreSQL database dump
--

-- Dumped from database version 14.4 (Debian 14.4-1.pgdg90+1)
-- Dumped by pg_dump version 14.4 (Debian 14.4-1.pgdg90+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: Personal; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Personal" (
    "Nombre" character varying(255),
    "Apellidos" character varying(255),
    "Identificacion" numeric(255,0) NOT NULL,
    "Celular" numeric(255,0)
);


ALTER TABLE public."Personal" OWNER TO postgres;

--
-- Name: Productos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Productos" (
    "Nombre" character varying(255),
    "Id" numeric,
    "Precio" character varying(255)
);


ALTER TABLE public."Productos" OWNER TO postgres;

--
-- Name: Proveedores; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Proveedores" (
    "Nombre" character varying(255),
    "Nit" numeric(255,0)
);


ALTER TABLE public."Proveedores" OWNER TO postgres;

--
-- Data for Name: Personal; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Personal" ("Nombre", "Apellidos", "Identificacion", "Celular") FROM stdin;
JOHANNA	NIETO	12345678	1333555
HECTOR	MARTINEZ	23456789	88552225
CLARA	SOSA	345687890	55471133
JOHN	RAMIREZ	63577811	45587415
\.


--
-- Data for Name: Productos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Productos" ("Nombre", "Id", "Precio") FROM stdin;
Disco Duro	1	580000
Equipo portatil	2	150000
Mouse	3	50000
Teclado	4	60000
Pantalla	5	250000
\.


--
-- Data for Name: Proveedores; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Proveedores" ("Nombre", "Nit") FROM stdin;
\.


--
-- PostgreSQL database dump complete
--

